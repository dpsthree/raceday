export const environment = {
  production: true,
  menuSP: '?actionName=GetMenuForAngular&parmNames=String:user_name&parmValues=ksmith&delimiter=~!~',
  horseSearch: '?actionName=HorseEntryInquiry&parmNames=String:sale_id~!~search_type~!~search_name&parmValues=80~!~N~!~1&delimiter=~!~',
  horseDetails: '?actionName=EntryDetail',
  baseUrl: 'http://192.168.201.6:8080/AngularServlet.do'
};
