// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.


export const environment = {
  production: false,
  menuSP: '?actionName=GetMenuForAngular&parmNames=String:user_name&parmValues=ksmith&delimiter=~!~',
  // tslint:disable-next-line:max-line-length
  horseSearch: '?actionName=HorseEntryInquiry',
  horseDetails: '?actionName=EntryDetail',
  baseUrl: 'http://192.168.201.6:8080/AngularServlet.do'
};
