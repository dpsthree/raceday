import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { RouteEntry } from './app.types';

@Injectable()
export class AuthServiceService implements CanActivate {

  roleMapping: { [key: string]: any } = {
    'accounting': [{ visibleName: 'Customers', route: 'customer' }, {visibleName: 'Reports', route: 'reports'}],
    'entries': [{ visibleName: 'Details', route: 'details' }],
    'assignments': {
      'pspears': ['accounting', 'entries']
    }
  };

  constructor() { }

  canActivate(route: ActivatedRouteSnapshot) {
    const routes = this.getRoutesForUser();
    return !!routes.find(r => r.route === route.routeConfig.path);
  }

  getMenuForUser() {
    return this.getRoutesForUser();
  }

  private getRoutesForUser() {
    const roles = this.roleMapping.assignments['pspears'];
    const routes: RouteEntry[] = [];
    roles.forEach((role: string) => {
      const routesForRole = this.roleMapping[role];
      routesForRole.forEach((r: RouteEntry) => routes.push(r));
    });
    return routes;
  }



}
