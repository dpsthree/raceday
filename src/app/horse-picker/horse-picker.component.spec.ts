import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HorsePickerComponent } from './horse-picker.component';

describe('HorsePickerComponent', () => {
  let component: HorsePickerComponent;
  let fixture: ComponentFixture<HorsePickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HorsePickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HorsePickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
