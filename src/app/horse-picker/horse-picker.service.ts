import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Subscription } from 'rxjs/Subscription';
import { filter, switchMap, shareReplay, distinctUntilChanged, debounceTime } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';

import { Horse } from '../app.types';
import { HorsePickerComponent } from './horse-picker.component';
import { Observable } from 'rxjs/Observable';
import { AppApiService } from '../app-api.service';
import _ = require('lodash');

@Injectable()
export class HorsePickerService {

  selectedHorse: Observable<Horse>;
  dialogSub: Subscription;

  constructor(private matDialog: MatDialog, route: ActivatedRoute, private router: Router, appApiService: AppApiService) {
    this.selectedHorse = route.queryParams
    .pipe(
      filter(queryParams => queryParams && queryParams.entry_detail_id),
      distinctUntilChanged(_.isEqual),
      debounceTime(1000),
      switchMap(queryParams => appApiService.getHorseDetails(queryParams)),
      shareReplay()
    );
  }

  openHorsePicker(currentRoute: ActivatedRoute) {
    const dialogRef = this.matDialog.open(HorsePickerComponent);

    if (this.dialogSub) {
      this.dialogSub.unsubscribe();
    }

    this.dialogSub = (dialogRef.afterClosed() as Observable<Horse>)
      .pipe(
        filter(horse => !!horse),
    )
      .subscribe(horse => this.router.navigate([], { relativeTo: currentRoute, queryParams: { entry_detail_id: horse.entry_detail_id } }));
  }

}
