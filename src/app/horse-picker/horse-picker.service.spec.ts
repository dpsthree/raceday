import { TestBed, inject } from '@angular/core/testing';

import { HorsePickerService } from './horse-picker.service';

describe('HorsePickerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HorsePickerService]
    });
  });

  it('should be created', inject([HorsePickerService], (service: HorsePickerService) => {
    expect(service).toBeTruthy();
  }));
});
