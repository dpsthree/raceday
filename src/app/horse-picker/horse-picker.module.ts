import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MatTableModule, MatCheckboxModule, MatDialogModule, MatButtonModule } from '@angular/material';

import { HorsePickerComponent } from './horse-picker.component';
import { HorsePickerService } from './horse-picker.service';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatTableModule,
    MatCheckboxModule,
    MatDialogModule,
    MatButtonModule
  ],
  providers: [HorsePickerService],
  declarations: [HorsePickerComponent],
  exports: [HorsePickerComponent],
  entryComponents: [HorsePickerComponent]
})
export class HorsePickerModule { }
