import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { SelectionModel } from '@angular/cdk/collections';

import { AppApiService } from '../app-api.service';
import { Horse } from '../app.types';

@Component({
  selector: 'kss-horse-picker',
  templateUrl: './horse-picker.component.html',
  styleUrls: ['./horse-picker.component.css']
})
export class HorsePickerComponent {
  horseRecord: Observable<any[]>;
  horseSearchForm: FormGroup;
  displayedColumns = ['select', 'dam_name', 'sire_name', 'hip_number', 'sex'];

  selection = new SelectionModel<Horse>(false, []);

  constructor(private appApi: AppApiService, formBuilder: FormBuilder) {
    this.horseSearchForm = formBuilder.group({
      sale_id: [''],
      search_type: [''],
      search_name: ['']
    });
  }

  submit() {
    this.horseRecord = this.appApi.getHorses(this.horseSearchForm.value);
  }


}
