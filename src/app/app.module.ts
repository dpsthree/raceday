import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { MatToolbarModule, MatButtonModule, MatDialogModule } from '@angular/material';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppApiService } from './app-api.service';
import { HorsePickerModule } from './horse-picker/horse-picker.module';
import { AuthServiceService } from './auth-service.service';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatDialogModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    HorsePickerModule
  ],
  providers: [
    AppApiService,
    AuthServiceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
