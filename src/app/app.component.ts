import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map, filter } from 'rxjs/operators';

import { HorsePickerService } from './horse-picker/horse-picker.service';
import { ActivatedRoute } from '@angular/router';
import { AuthServiceService } from './auth-service.service';

@Component({
  selector: 'kss-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  selectedHorse: Observable<string>;
  menuList: {visibleName: string, route: string}[];
  constructor(private horsePickerService: HorsePickerService, private activatedRoute: ActivatedRoute, authService: AuthServiceService) {
    this.selectedHorse = this.horsePickerService.selectedHorse
      .pipe(
        filter(horse => !!horse),
        map(horse => horse.horse_name),
        map(name => {
          return name ? name : 'Unnamed';
        })
      );

    this.menuList = authService.getMenuForUser();
  }

  openDialog() {
    this.horsePickerService.openHorsePicker(this.activatedRoute);
  }
}
