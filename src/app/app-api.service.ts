import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../environments/environment';
import { ParamObject, Horse } from './app.types';
import { convertParamsToSPString } from './sp-conversion';
import { map } from 'rxjs/operators';

const BASE_URL = environment.baseUrl;
const MENU_URL = BASE_URL + environment.menuSP;
const HORSE_SEARCH = BASE_URL + environment.horseSearch;
const HORSE_DETAILS = BASE_URL + environment.horseDetails;

@Injectable()
export class AppApiService {

  constructor(private httpClient: HttpClient) { }

  getMenu() {
    // tslint:disable-next-line:max-line-length
    return this.httpClient.get<any>(MENU_URL);
  }

  getHorses(requestParams: ParamObject) {
    const spParams = convertParamsToSPString(requestParams);
    return this.httpClient.get<Horse[]>(HORSE_SEARCH + spParams);
  }

  getHorseDetails(requestParams: ParamObject) {
    const spParams = convertParamsToSPString(requestParams);
    return this.httpClient.get<Horse[]>(HORSE_DETAILS + spParams)
      .pipe(
        map(horseList => horseList[0])
      );
  }

}
