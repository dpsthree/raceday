import { Component } from '@angular/core';
import { HorsePickerService } from '../horse-picker/horse-picker.service';
import { Observable } from 'rxjs/Observable';
import { Horse } from '../app.types';

@Component({
  selector: 'kss-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent {

  details: Observable<Horse>;
  constructor(pickerService: HorsePickerService) {
    this.details = pickerService.selectedHorse;
  }
}
