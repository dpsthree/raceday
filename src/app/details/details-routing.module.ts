import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailsComponent } from './details.component';
import { EditComponent } from './edit/edit.component';
import { AuthServiceService } from '../auth-service.service';

const routes: Routes = [
  { path: '', component: DetailsComponent },
  { path: 'edit/:feature', component: EditComponent, canActivate: [AuthServiceService] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DetailsRoutingModule { }
