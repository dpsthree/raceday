import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailsRoutingModule } from './details-routing.module';
import { DetailsComponent } from './details.component';
import { HorsePickerModule } from '../horse-picker/horse-picker.module';
import { EditComponent } from './edit/edit.component';

@NgModule({
  imports: [
    CommonModule,
    DetailsRoutingModule,
    HorsePickerModule
  ],
  declarations: [DetailsComponent, EditComponent]
})
export class DetailsModule { }
