export interface ParamObject {
  [key: string]: string;
}

export interface Horse {
  horse_name: string;
  dam_name: string;
  sire_name: string;
  hip_number: string;
  sex: string;
  entry_detail_id: string;
}

export interface RouteEntry {
  visibleName: string;
  route: string;
}
