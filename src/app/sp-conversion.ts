import * as _ from 'lodash';

import { ParamObject } from './app.types';

export function convertParamsToSPString(params: ParamObject) {
  const prefix = '&parmNames=';
  const suffix = '&delimiter=~!~';
  const midfix = '&parmValues=';
  let keyString = '';
  let valueString = '';
  let iteration = 0;
  _.forEach(params, (value, key) => {
    if (iteration !== 0) {
      keyString += '~!~';
      valueString += '~!~';
    }
    keyString += 'String:' + key;
    valueString += value;
    iteration++;
  });
  return prefix + keyString + midfix + valueString + suffix;
}
