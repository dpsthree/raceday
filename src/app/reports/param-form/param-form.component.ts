import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { ReportService } from '../report.service';
import { ReportParameter } from '../reports.types';

@Component({
  selector: 'kss-param-form',
  templateUrl: './param-form.component.html',
  styleUrls: ['./param-form.component.css']
})
export class ParamFormComponent {

  parameterList: Observable<ReportParameter[]>;
  parameterForm: FormGroup;

  constructor(report: ReportService, activatedRoute: ActivatedRoute, private formBuilder: FormBuilder) {
    this.parameterList = report.getSelectedReport(activatedRoute)
      .pipe(
        map(id => report.getParamsForId(id)),
        tap(paramList => this.generateForm(paramList))
      );
  }

  generateForm(paramList: ReportParameter[]) {
    const formObject: any = {};
    paramList.forEach(param => {
      const formEntry: any[] = [''];
      if (param.required) {
        formEntry.push(Validators.required);
      }
      formObject[param.paramId] = formEntry;
    });
    this.parameterForm = this.formBuilder.group(formObject);
  }


}
