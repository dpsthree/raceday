import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportsComponent } from './reports.component';
import { ParamFormComponent } from './param-form/param-form.component';

const routes: Routes = [
  {
    path: '', component: ReportsComponent, children: [
      {path: ':reportId', component: ParamFormComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
