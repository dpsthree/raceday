import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { pluck } from 'rxjs/operators';

@Injectable()
export class ReportService {

  reports = [
    {
      menuEntry: 'Horse Type List',
      reportIdentifier: 'horseTypeList',
      url: 'abacdfsdfs',
      parameters: [
        {
          label: 'Sale ID',
          paramId: 'saleId',
          uiType: 'dropdown',
          required: '',
          options: [
            { display: 'Sept 2018', value: 80 },
            { display: 'Jan 2018', value: 79 }
          ]
        }
      ]
    }
  ];


  constructor() { }

  getSelectedReport(activatedRoute: ActivatedRoute) {
    return activatedRoute.params
      .pipe(
        pluck<any, string>('reportId')
      );
  }

  getParamsForId(reportId: string) {
    const report = this.reports.find(r => r.reportIdentifier === reportId);
    return report.parameters;
  }

}
