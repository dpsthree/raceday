import { Component } from '@angular/core';
import { ReportService } from './report.service';
import { ReportDefinition } from './reports.types';

@Component({
  selector: 'kss-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent {

  reports: ReportDefinition[];
  constructor(reportService: ReportService) {
    this.reports = reportService.reports;
  }
}
