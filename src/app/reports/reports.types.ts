export interface ReportDefinition {
  menuEntry: string;
  reportIdentifier: string;
  url: string;
  parameters: ReportParameter[];
}

export interface ReportParameter {
  label: string;
  paramId: string;
  uiType: string;
  required: string;
  options: { display: string, value: any }[];
}
