import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportsRoutingModule } from './reports-routing.module';
import { ReportsComponent } from './reports.component';
import { ReportService } from './report.service';
import { ParamFormComponent } from './param-form/param-form.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ReportsRoutingModule,
    ReactiveFormsModule
  ],
  providers: [ReportService],
  declarations: [ReportsComponent, ParamFormComponent]
})
export class ReportsModule { }
