import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthServiceService } from './auth-service.service';

const routes: Routes = [
  {path: 'details', loadChildren: './details/details.module#DetailsModule', canActivate: [AuthServiceService]},
  {path: 'customer', loadChildren: './customer/customer.module#CustomerModule', canActivate: [AuthServiceService]},
  {path: 'reports', loadChildren: './reports/reports.module#ReportsModule', canActivate: [AuthServiceService]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
